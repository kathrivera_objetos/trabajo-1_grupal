package helpers;
public class Catedratico extends Persona{
    public Catedratico(String nombre, String apellido, String edad, String telefono){
        super(nombre,apellido,edad,telefono);
    }
    public String profesion() {
        return ("CATEDRATICO");
    }
    public String imprimir_Datos() {
        return "Nombre: "+this.getNombre()+" "+this.getApellido()+"\nEdad: "+this.getEdad()+"\nTelefono: "+this.getTelefono()+"\nProfesion: "+this.profesion();
    }
}
