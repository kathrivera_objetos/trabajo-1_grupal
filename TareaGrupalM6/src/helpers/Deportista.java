/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

/**
 *
 * @author Fernando Portillo
 */
public class Deportista extends Persona {
    public Deportista(){
        this.setNombre("Fernando");
        this.setApellido("Portillo");
        this.setEdad("20");
        this.setTelefono("98007856");
    } 
    public String profesion() {
        return ("DEPORTISTA");
    }
    public String imprimir_Datos() {
        return "Nombre: "+this.getNombre()+" "+this.getApellido()+"\nEdad: "+this.getEdad()+"\nTelefono: "+this.getTelefono()+"\nProfesion: "+this.profesion();
    }
}
