package tareagrupalm6;
import helpers.Catedratico;
import helpers.Deportista;
import helpers.Doctor;
import helpers.Persona;
public class TareaGrupalM6 {
    public static void main(String[] args) {
        Persona[] persona = new Persona[3];
        persona[0] =new Doctor();
        persona[1] = new Deportista();
        persona[2] = new Catedratico("Katherine","Rivera","22","3204-3963");
        
        for(int j=0;j<persona.length;j++){
            System.out.println(persona[j].imprimir_Datos()+"\n--------------------");
        }
    }
}
